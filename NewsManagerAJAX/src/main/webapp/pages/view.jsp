<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<a href="#" onclick="sendRequest('newsAction.do', 'method=news');">
	<bean:message key="label.common.news" />
</a>
<bean:message key="label.common.section.view" />
<table id="view">
	<tr>
		<td id="view-first-column"><bean:message
				key="label.common.table.title" /></td>
		<td id="view-second-column"><bean:write name="newsForm"
				property="currentNews.title" /></td>
	</tr>
	<tr>
		<td id="view-first-column"><bean:message
				key="label.common.table.date" /></td>
		<td id="view-second-column"><bean:write name="newsForm"
				property="currentNews.date" formatKey="format.date" /></td>
	</tr>
	<tr>
		<td id="view-first-column"><bean:message
				key="label.common.table.brief" /></td>
		<td id="view-second-column"><bean:write name="newsForm"
				property="currentNews.brief" /></td>
	</tr>
	<tr>
		<td id="view-first-column"><bean:message
				key="label.common.table.content" /></td>
		<td id="view-second-column"><bean:write name="newsForm"
				property="currentNews.content" /></td>
	</tr>
</table>
<div id="delete">
	<form onsubmit="if(confirmDeleting()){sendRequest('newsAction.do','method=delete&ID=${newsForm.currentNews.id}');return true;}else{return false;}"
			 style="display: inline;">
		<input type="submit" id="button-delete"
				value=<bean:message key="label.common.button.delete" />>
	</form>
	<form onsubmit="sendRequest('newsAction.do','method=edit&ID=${newsForm.currentNews.id}');"
			 style="display: inline;">
		<input type="submit" id="button-delete" 
				value=<bean:message key="label.common.button.edit"/>>
	</form>
</div>