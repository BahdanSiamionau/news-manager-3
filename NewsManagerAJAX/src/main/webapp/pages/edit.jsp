<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<form onsubmit="if(validate(this)){saveNewsRequestWrapper(this, 'newsAction.do', 'method=update');return true;}else{return false;}"
		style="display: inline;">
	<a href="#" onclick="sendRequest('newsAction.do', 'method=news');">
		<bean:message key="label.common.news" />
	</a>
	<logic:equal name="isAddPage" value="true">
		<bean:message key="label.common.section.add" />
	</logic:equal>
	<logic:equal name="isEditPage" value="true">
		<bean:message key="label.common.section.edit" />
	</logic:equal>
	<table id="view">
		<tr>
			<td id="view-first-column">
				<bean:message key="label.common.table.title" />
			</td>
			<td id="view-second-column">
				<input type="text" name="title" id="text-title-style"
						value="${newsForm.currentNews.title}" />
			</td>
		</tr>
		<tr>
			<td id="view-first-column">
				<bean:message key="label.common.table.date" />
			</td>
			<td id="view-second-column">
				<input type="text" name="date"
						value=<bean:write name="newsForm" property="currentNews.date" formatKey="format.date"/>>
				
			</td>
		</tr>
		<tr>
			<td id="view-first-column">
				<bean:message key="label.common.table.brief" />
			</td>
			<td id="view-second-column">
				<textarea name="brief" id="textarea-brief-style">${newsForm.currentNews.brief}</textarea>
			</td>
		</tr>
		<tr>
			<td id="view-first-column">
				<bean:message key="label.common.table.content" />
			</td>
			<td id="view-second-column">
				<textarea name="content" id="textarea-content-style">${newsForm.currentNews.content}</textarea>
			</td>
		</tr>
	</table>
	<input type="submit" id="edit-button-style" style="margin-left: 350px;"
				value=<bean:message key="label.common.button.update" />>
</form>
<form onsubmit="sendRequest('newsAction.do','method=previous');" style="display: inline;">
	<input type="submit" id="edit-button-style"
			value=<bean:message key="label.common.button.cancel" />>
</form>