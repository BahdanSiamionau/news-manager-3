<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<a href="#" onclick="sendRequest('newsAction.do', 'method=news');">
	<bean:message key="label.common.news"/>
</a>
<bean:message key="label.common.section.list"/>
<div id="news-list">
	<form onsubmit="if(confirmDeleting()){deleteNewsRequestWrapper('newsAction.do','method=delete');return true;}else{return false;}">
		<logic:iterate id="element" name="newsForm" property="newsList">
			<br>
			<div id="news-list-title">
				<bean:write name="element" property="title" />
			</div>
			<div id="news-list-date">
				<bean:write name="element" property="date" formatKey="format.date" />
			</div>
			<br>
			<div id="news-list-brief">
				<bean:write name="element" property="brief" />
			</div>
			<div id="control">
				<a href="#" onclick="sendRequest('newsAction.do', 'method=view&ID=${element.id}');">
					<bean:message key="label.common.viewtitle" />
				</a>
				<a href="#" onclick="sendRequest('newsAction.do', 'method=edit&ID=${element.id}');">
					<bean:message key="label.common.edit" />
				</a>
				<input type="checkbox" name="checkbox" value="${element.id}">
			</div>
			<br>
		</logic:iterate>
		<logic:notEmpty name="newsForm" property="newsList">
			<div id="delete">
				<input type='submit' id="button-delete" 
						value=<bean:message key="label.common.button.delete"/>>
			</div>
		</logic:notEmpty>
	</form>
</div>