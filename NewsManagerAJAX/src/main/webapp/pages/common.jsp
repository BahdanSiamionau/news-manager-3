<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<html> 
	<head>
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" />
		<link rel="stylesheet" href="resources/css/loader.css" type="text/css" />
		<script src="resources/js/ajax.js" type="text/javascript" charset="utf-8"></script>
		<script src="resources/js/constants.js" type="text/javascript" charset="utf-8"></script>
		<script src="resources/js/validator.js" type="text/javascript" charset="utf-8"></script>
		<script src="resources/js/confirmator.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			var messages = {
				'error.message.required.title' : '<bean:message key="error.message.required.title" />',
				'error.message.required.date' : '<bean:message key="error.message.required.date" />',
				'error.message.required.brief' : '<bean:message key="error.message.required.brief" />',
				'error.message.required.content' : '<bean:message key="error.message.required.content" />',
				'error.message.maxlength.title' : '<bean:message key="error.message.maxlength.title" />',
				'error.message.maxlength.brief' : '<bean:message key="error.message.maxlength.brief" />',
				'error.message.maxlength.content' : '<bean:message key="error.message.maxlength.content" />',
				'error.message.format.date' : '<bean:message key="error.message.format.date" />',
				'format.date' : '<bean:message key="format.date" />',
				'confirm.delete': "<bean:message key="confirm.delete" />",
				'error.message.nothing.checked' : "<bean:message key="error.message.nothing.checked" />"
			};
		</script>
	</head>
	<body onload="content=document.getElementById('content');sendRequest('newsAction.do', 'method=${newsForm.currentAction}');">
		<div id="head">
			<h1>
				<bean:message key="label.common.header" />
			</h1>
			<html:link page="/newsAction.do?method=language&locale=EN">
				<bean:message key="label.common.english" />
			</html:link>
			<html:link page="/newsAction.do?method=language&locale=RU">
				<bean:message key="label.common.russian" />
			</html:link>
		</div>
		<div id="menu">
			<h2>
				<bean:message key="label.common.news" />
			</h2>
			<ul>
				<li>
					<a href="#" onclick="sendRequest('newsAction.do', 'method=news');">
						<bean:message key="label.common.list"/>
					</a>
				</li>
				<li>
					<a href="#" onclick="sendRequest('newsAction.do', 'method=add');">
						<bean:message key="label.common.addnews" />
					</a>
				</li>
			</ul>
		</div>
		<div id="content">
		
		</div>
		<div id="foot">
			<bean:message key="label.common.footer" />
		</div>
	</body>
</html>