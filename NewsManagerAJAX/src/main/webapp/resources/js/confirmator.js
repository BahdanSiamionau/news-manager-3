function confirmDeleting() {

	var checkboxes = document.getElementsByName('checkbox');

	if (checkboxes.length == 0) {
		var agree = confirm(messages['confirm.delete']);
		if (agree) {
			return true;
		} else {
			return false;
		}
	} else {
		var result = false;
		for ( var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				result = true;
				break;
			}
		}
		if (!result) {
			alert(messages['error.message.nothing.checked']);
			return false;
		}
		var agree = confirm(messages['confirm.delete']);
		if (agree) {
			return true;
		} else {
			return false;
		}
	}
}