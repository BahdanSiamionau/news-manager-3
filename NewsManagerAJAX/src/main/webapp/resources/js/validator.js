function validate(form) {
	var title = form.title.value;
	var date = form.date.value;
	var brief = form.brief.value;
	var content = form.content.value;
	var error = "";
	if (title.length == 0) {
		error += messages['error.message.required.title'] + '\n';
	} else if (title.length > 120) {
		error += messages['error.message.maxlength.title'] + '\n';
	}
	if (date.length == 0) {
		error += messages['error.message.required.date'] + '\n';
	} else {
		var dateFormat = messages['format.date'] + '\n';
		var re;
		if (dateFormat[0] == 'M') {
			re = /^(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])\/(197[1-9]|19[89][0-9]|200[0-9]|201[0123])$/;
		} else {
			re = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(197[1-9]|19[89][0-9]|200[0-9]|201[0123])$/;
		}
		if (!re.test(date)) {
			error += messages['error.message.format.date'] + '\n';
		}
	}
	if (brief.length == 0) {
		error += messages['error.message.required.brief'] + '\n';
	} else if (brief.length > 600) {
		error += messages['error.message.maxlength.brief'] + '\n';
	}
	if (content.length == 0) {
		error += messages['error.message.required.content'] + '\n';
	} else if (content.length > 3000) {
		error += messages['error.message.maxlength.content'] + '\n';
	}
	if (error == '') {
		return true;
	} else {
		alert(error);
		return false;
	}
}