var req;
var content;

function getXMLHTTPRequest() {
	var xRequest = null;
	if (window.XMLHttpRequest) {
		xRequest = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		xRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xRequest;
}

function sendRequest(url, params, HttpMethod) {
	if (!HttpMethod) {
		HttpMethod = "POST";
	}
	req = getXMLHTTPRequest();
	if (req){
		req.onreadystatechange = onReadyStateChange;
		req.open(HttpMethod,url,true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(params);
	}
}

function onReadyStateChange() {
	var ready = req.readyState;
	var data = null;
	if (ready == READY_STATE_COMPLETE){
		data = req.responseText;
	} else {
		data = LOADER_TAG;
	}
	setContent(data);
}

function setContent(data){
	if (content != null) {
		content.innerHTML = data;
	}
}

function deleteNewsRequestWrapper(url, params, HttpMethod) {
	var checkboxes = document.getElementsByName("checkbox");
	params += "&ID=";
	for (var i = 0; i < checkboxes.length; i++){
		if (checkboxes[i].checked) {
			params += checkboxes[i].value + ",";
		}
	}
	if (!HttpMethod) {
		HttpMethod = "POST";
	}
	req = getXMLHTTPRequest();
	if (req){
		req.onreadystatechange = onReadyStateChange;
		req.open(HttpMethod,url,true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(params);
	}
}

function saveNewsRequestWrapper(form, url, params, HttpMethod) {
	for (var i = 0; i < form.length; i++) {
		if (form[i].name != "") {
			params += "&" + form[i].name + "=" + form[i].value;
		}
	}
	if (!HttpMethod) {
		HttpMethod = "POST";
	}
	req = getXMLHTTPRequest();
	if (req){
		req.onreadystatechange = onReadyStateChange;
		req.open(HttpMethod,url,true);
		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		req.send(params);
	}
}