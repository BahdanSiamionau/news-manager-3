package com.epam.news3.action;

import java.sql.Date;

import java.text.SimpleDateFormat;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.epam.news3.dao.INewsDAO;
import com.epam.news3.form.NewsForm;
import com.epam.news3.model.News;
import com.epam.news3.resource.NewsConstant;

public final class NewsAction extends DispatchAction {
	
	private static final String COMMA = ",";
	private static final String ADD = "add";
	private static final String VIEW = "view";
	private static final String NEWS = "news";
	private static final String EDIT = "edit";
	private static final String COMMON = "common";
	private static final String LOCALE = "locale";
	private static final String IS_ADD_PAGE = "isAddPage";
	private static final String IS_EDIT_PAGE = "isEditPage";
	private static final String STR_TRUE = "true";
	private static final String STR_FALSE = "false";
	private static final String LABELS = "labels";
	private static final String FORMAT_DATE = "format.date";
	
	private INewsDAO newsDAO;
	
	public ActionForward init(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(COMMON);
	}

	public ActionForward news(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setNewsList(newsDAO.getNewsList());
		newsForm.setCurrentAction(NEWS);
		newsForm.setPreviousAction(NEWS);
		return mapping.findForward(NEWS);
	}

	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		String id = request.getParameter(NewsConstant.ID);
		if (id != null) {
			newsForm.setCurrentNews(newsDAO.getCurrentNews(Integer.parseInt(id)));
		} 
		newsForm.setCurrentAction(VIEW);
		newsForm.setPreviousAction(VIEW);
		return mapping.findForward(VIEW);
	}
	
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentNews(new News());
		request.getSession().setAttribute(IS_ADD_PAGE, STR_TRUE);
		request.getSession().setAttribute(IS_EDIT_PAGE, STR_FALSE);
		newsForm.setCurrentAction(ADD);
		newsForm.setPreviousAction(NEWS);
		return mapping.findForward(EDIT);
	}

	public ActionForward edit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		request.getSession().setAttribute(IS_ADD_PAGE, STR_FALSE);
		request.getSession().setAttribute(IS_EDIT_PAGE, STR_TRUE);
		String id = request.getParameter(NewsConstant.ID);
		if (id != null) {
			newsForm.setCurrentNews(newsDAO.getCurrentNews(Integer.parseInt(id)));
		}
		newsForm.setCurrentAction(EDIT);
		return mapping.findForward(EDIT);
	}
	
	public ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		ResourceBundle messages = ResourceBundle.getBundle(LABELS, (Locale)request.getSession().getAttribute(Globals.LOCALE_KEY));
		SimpleDateFormat dateFormat = new SimpleDateFormat(messages.getString(FORMAT_DATE));
		News news = newsForm.getCurrentNews();
		news.setTitle(request.getParameter(NewsConstant.TITLE.toLowerCase()));
		news.setDate(new Date(dateFormat.parse(request.getParameter(NewsConstant.DATE.toLowerCase())).getTime()));
		news.setBrief(request.getParameter(NewsConstant.BRIEF.toLowerCase()));
		news.setContent(request.getParameter(NewsConstant.CONTENT.toLowerCase()));
		if (STR_TRUE.equals(request.getSession().getAttribute(IS_EDIT_PAGE))) {
			newsDAO.editNews(news);
		} else {
			newsDAO.addNews(news);
		}
		return new ActionForward("/newsAction.do?method=view&id=" + news.getId());
	}
	
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String idListString = request.getParameter(NewsConstant.ID);
		String[] idListStringArray = idListString.split(COMMA);
		Integer[] selected = new Integer[idListStringArray.length];
		for (int i = 0; i < selected.length; i++) {
			selected[i] = Integer.parseInt(idListStringArray[i]);
		}
		newsDAO.removeNews(selected);
		return new ActionForward("/newsAction.do?method=news");
	}
	
	public ActionForward language(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		request.getSession().setAttribute(Globals.LOCALE_KEY, new Locale((String)request.getParameter(LOCALE)));
		return mapping.findForward(COMMON);
	}
	
	public ActionForward previous(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {
		NewsForm newsForm = (NewsForm) form;
		newsForm.setCurrentAction(newsForm.getPreviousAction());
		return mapping.findForward(newsForm.getPreviousAction());
	}
	
	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
}