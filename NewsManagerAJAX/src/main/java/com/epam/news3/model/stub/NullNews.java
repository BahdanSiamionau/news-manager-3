package com.epam.news3.model.stub;

import java.sql.Date;

import com.epam.news3.model.News;

public class NullNews extends News {

	private static final long serialVersionUID = 1L;
	private static final int NULL_INTEGER = 0;
	private static final String NULL_STRING = "";
	private static final Date NULL_DATE = new Date(0);

	@Override
	public int getId() {
		return NULL_INTEGER;
	}
	
	@Override
	public void setId(int id) {}
	
	@Override
	public String getTitle() {
		return NULL_STRING;
	}
	
	@Override
	public void setTitle(String title) {}

	@Override
	public Date getDate() {
		return NULL_DATE;
	}
	
	@Override
	public void setDate(Date date) {}
	
	@Override
	public String getBrief() {
		return NULL_STRING;
	}
	
	@Override
	public void setBrief(String brief) {}
	
	@Override
	public String getContent() {
		return NULL_STRING;
	}
	
	@Override
	public void setContent(String content) {}
}
