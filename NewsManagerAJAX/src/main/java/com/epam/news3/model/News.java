package com.epam.news3.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;

import java.sql.Date;

@Entity
@NamedQueries({
	@NamedQuery(name="select_all", query ="select c from News c order by c.date desc"),
	@NamedQuery(name="delete", query ="delete from News c WHERE c.id in (:ID)")
}) 
@Table(name="NEWSTABLE")
public class News implements Serializable {
	
	static private final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private Date date = new Date(System.currentTimeMillis());
	private String brief;
	private String content;
	
	@Id
	@SequenceGenerator(name = "ID", sequenceName = "NEWS_INCREMENT")
	@GeneratedValue(generator = "ID")
	@Column(name = "ID")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "TITLE")
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "NEWS_DATE")
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(name = "BRIEF")
	public String getBrief() {
		return brief;
	}
	
	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	@Column(name = "CONTENT")
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
}
