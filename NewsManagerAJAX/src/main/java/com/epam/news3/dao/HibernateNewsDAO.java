package com.epam.news3.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.epam.news3.exception.NewsManagerException;
import com.epam.news3.model.News;
import com.epam.news3.resource.NewsConstant;

public final class HibernateNewsDAO implements INewsDAO {
	
	private static final String SELECTED = "selected";
	private static final String HQL_SELECT_ALL = "hibernate_select_all_query";
	private static final String HQL_SELECT = "hibernate_select_query";
	private static final String HQL_DELETE = "hibernate_delete_query";
	
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<News> getNewsList() throws NewsManagerException {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.getNamedQuery(HQL_SELECT_ALL);
		List <News> list = query.list();
		session.getTransaction().commit();
		return new ArrayList <News> (list);
	}

	@Override
	public News getCurrentNews(int id) throws NewsManagerException {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.getNamedQuery(HQL_SELECT);
		query.setParameter(NewsConstant.ID, id);
		News news = (News) query.uniqueResult();
		session.getTransaction().commit();
	    return news;
	}

	@Override
	public void addNews(News news) throws NewsManagerException {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(news);
		session.getTransaction().commit();
	}

	@Override
	public void editNews(News news) throws NewsManagerException {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.update(news);
		session.getTransaction().commit();
	}

	@Override
	public void removeNews(Integer[] selectedItems) throws NewsManagerException {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.getNamedQuery(HQL_DELETE);
		query.setParameterList(SELECTED, selectedItems);
		query.executeUpdate();
		session.getTransaction().commit();
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
