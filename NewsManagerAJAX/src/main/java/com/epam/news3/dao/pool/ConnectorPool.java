package com.epam.news3.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.concurrent.ArrayBlockingQueue;

import org.apache.log4j.Logger;

import com.epam.news3.exception.NewsManagerException;

public final class ConnectorPool {

	private static final int MAX_CONNECTORS = 10;	
	private static final Logger logger = Logger.getLogger(ConnectorPool.class);
	
	private String driverName;
	private String url;
	private String user;
	private String password;
	private ArrayBlockingQueue <Connector> connectors;
	
	public void init() {
		try {
			Class.forName(this.driverName);
			connectors = new ArrayBlockingQueue<Connector>(MAX_CONNECTORS);
			for (int i = 0; i < MAX_CONNECTORS; i++) {
				Connection connection = DriverManager.getConnection(this.url, this.user, this.password);
				Connector connector = new Connector(this, connection);
				connectors.add(connector);
			}
		} catch (SQLException | ClassNotFoundException ex) {
			logger.error(ex.getMessage());
		}
	}

	public Connector take() throws NewsManagerException {
		try {
			Connector connector = connectors.take();
			return connector;
		} catch (InterruptedException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}

	public synchronized void release(Connector connector) {
		connectors.add(connector);
	}
	
	public String getDriverName() {
		return driverName;
	}
	
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
