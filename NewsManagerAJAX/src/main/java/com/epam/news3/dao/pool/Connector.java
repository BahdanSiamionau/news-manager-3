package com.epam.news3.dao.pool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.news3.exception.NewsManagerException;

public final class Connector implements AutoCloseable {
	
	private static final String EXCEPTION_NULL_CONN = "Connection is null";
	private static final Logger logger = Logger.getLogger(Connector.class);
	
	private ConnectorPool pool;
	private Connection connection;

	public Connector(ConnectorPool pool, Connection connection) {
		this.pool = pool;
		this.connection = connection;
	}
	
	public Statement getStatement() throws NewsManagerException {
		try {
			if (connection != null) {
				return connection.createStatement();
			}
			throw new NewsManagerException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	public PreparedStatement getPreparedStatement(String query) throws NewsManagerException {
		try {
			if (connection != null) {
				return connection.prepareStatement(query);
			}
			throw new NewsManagerException(EXCEPTION_NULL_CONN);
		} catch (SQLException ex) {
			logger.error(ex.getMessage());
			throw new NewsManagerException(ex);
		}
	}
	
	public void close() {
		pool.release(this);
	}
}