package com.epam.news3.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.epam.news3.model.News;


@SuppressWarnings("serial")
public final class NewsForm extends ActionForm {

	private ArrayList <News> newsList;
	private News currentNews;
	private String currentAction = "news";
	private String previousAction;
	
	public String getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
	}

	public String getPreviousAction() {
		return previousAction;
	}

	public void setPreviousAction(String previousAction) {
		this.previousAction = previousAction;
	}

	public ArrayList <News> getNewsList() {
		return newsList;
	}

	public void setNewsList(ArrayList <News> newsList) {
		this.newsList = newsList;
	}

	public News getCurrentNews() {
		return currentNews;
	}

	public void setCurrentNews(News currentNews) {
		this.currentNews = currentNews;
	}
}