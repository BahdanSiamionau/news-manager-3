package com.epam.news3.resource;

public final class NewsConstant {

	public static final String ID = "ID";
	public static final String TITLE = "TITLE";
	public static final String DATE = "DATE";
	public static final String NEWS_DATE = "NEWS_DATE";
	public static final String BRIEF = "BRIEF";
	public static final String CONTENT = "CONTENT";
	
	private NewsConstant() {
		
	}
}
